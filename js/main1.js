/*
HW 
(Клас - конструктор)

Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
Dog, Cat, Horse перевизначають методи makeNoise, eat.
Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.
Створіть клас Ветеринар, у якому визначте метод void (Нічого не повертає) treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте їх на прийом до ветеринара.
*/

class Animal {
  animal;
  food = 'вода';
  location = 'комната';
  constructor(food, location) {
    this.food = food;
    this.location = location;
  }
  makeNoise() {
    console.log(`${this.animal} спит`);
  }
  eat() {
    console.log(`${this.animal} породы ${this.breed}, любит есть ${this.food}`);
  }
  sleep() {
    console.log(`${this.animal} должна спать больше 8 часов в день`);
  }
}

class Dog extends Animal {
  animal = 'собака';
  breed;
  weight;
  constructor(breed, food, location, weight) {
    super(food, location);
    this.breed = breed;
    this.weight = weight;
  }
  makeNoise() {
    console.log(`${this.animal} гавкает`);
  }
  eat() {
    super.eat();
    console.log(`${this.animal} не любит цитрусовые`);
  }
}

class Cat extends Animal {
  animal = 'кошка';
  breed;
  tailLength;
  constructor(breed, food, location, tailLength) {
    super(food, location);
    this.breed = breed;
    this.tailLength = tailLength;
  }
  makeNoise() {
    console.log(`${this.animal} мяукает`);
  }
  eat() {
    super.eat();
    console.log(`${this.animal} должна регулярно получать пасту для отрыгивание шерсти`);
  }
}

class Horse extends Animal {
  animal = 'лошадь';
  breed;
  height;
  constructor(breed, food, location, height) {
    super(food, location);
    this.breed = breed;
    this.height = height;
  }
  makeNoise() {
    console.log(`${this.animal} ржет`);
  }
  eat() {
    super.eat();
    console.log(`${this.animal} любит ${this.food}`);
  }
}

let spaniel = new Dog(`Спаниель`, `сбалансированный корм`, `кухня`, 12);
let shepherd = new Dog(`Овчарка`, `мясо`, `двор`, 18);
let pincher = new Dog(`Пинчер`, `консервы`, `коридор`, 4);
let cat1 = new Cat(`Британец`, `рыба`, `комната`, 30);
let cat2 = new Cat(`Мейн-кун`, `мясо`, `крыша`, 50);
let cat3 = new Cat(`Сиамская`, `каша`, `веранда`, 40);
let brownHorse = new Horse(`Абтенайская`, `сено`, `стойло`, 850);
let blackHorse = new Horse(`Комтойс`, `свежая трава`, `иподром`, 1000);
let unicorn = new Horse(`Единорог`, `Сникерс`, `радуга`, 100);

spaniel.makeNoise();
spaniel.eat();
spaniel.sleep();

cat1.makeNoise();
cat1.eat();
cat1.sleep();

unicorn.makeNoise();
unicorn.eat();
unicorn.sleep();



class Veterinarian {
  treatAnimal(anim) {
    return document.write(`У вас на приёме ${anim.animal} породы ${anim.breed}, послединй раз животное ело ${anim.food} <br> Хозяин обнаружил признаки ухудшеня здоровья, когда животное было на/в ${anim.location} <br> <br> <br>`);
  }

  main() {
    let animals = [spaniel, shepherd, cat1, blackHorse, pincher, cat3, unicorn, cat2, brownHorse];
    return animals.map((client) => this.treatAnimal(client));
  }
}



vet = new Veterinarian();
vet.main();